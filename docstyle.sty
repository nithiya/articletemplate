%% docstyle.sty
%% Repository: https://gitlab.com/nithiya/articletemplate
%% Copyright 2021 N. M. Streethran (nmstreethran at gmail dot com)
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is N. M. Streethran.
%
% This work consists of the following files: the styles in
% docstyle.sty and the main configurations in docmain.tex.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% to avoid overfull margins
\emergencystretch3em

% set date format
\usepackage[british]{datetime2}

% colours
\usepackage[svgnames]{xcolor}

% author and title definitions
\author{%
  \theauthor
  \ifdefined\authornote\footnote{\authornote}\fi
  \ifdefined\secondnote\,\footnote{\secondnote}\fi
}
\title{\thetitle}

% to add spaces between paragraphs
\usepackage[indent]{parskip}
% to remove indentation from abstract
\ifundef{\abstract}{}{\patchcmd{\abstract}
  {\quotation}{\quotation\noindent\ignorespaces}{}{}}
% line spacing
\usepackage{setspace}
% bullet points and lists
\usepackage[inline]{enumitem}
\setlist{itemsep=.5pt}
% endnotes
% \usepackage{enotez}
% \setenotez{totoc={auto}}

% fonts and math
\usepackage{mathtools}
% choose a font set to use
% first font set
\usepackage{euler}
\usepackage[nomath]{libertinus}
\usepackage[scale=.85,lining,nomap]{FiraMono}
% second font set
% \usepackage{newpxtext}
% \usepackage[zerostyle=c,straightquotes]{newtxtt}
% math font - either eulerpx (upright font) or newpxmath
% \usepackage{newpxmath}

% boxes and icons
\usepackage{framed}
\usepackage{tcolorbox}
\usepackage{fontawesome5}

% listings
\usepackage{minted}
% define new environment for long listings with page breaks
\newenvironment{longlisting}{\captionsetup{type=listing}}{}

% tables and graphics
\usepackage{longtable}
\usepackage{booktabs}
\usepackage{graphicx}
% table font size
\let\oldlongtable\longtable
\renewcommand{\longtable}{\footnotesize\oldlongtable}
\usepackage[font=small,labelfont=bf]{caption}
% set default table and figure placement to !htb
\makeatletter
\def\fps@table{!htb}
\def\fps@figure{!htb}
\makeatother
% for subfigures
\usepackage{subcaption}
% for PDFs
% \usepackage{pdfpages}

% bibliography
% \usepackage[%
%   language=british,sorting=none,alldates=comp,dateabbrev=false,
%   giveninits=true,defernumbers=true,maxbibnames=7]{biblatex}
\usepackage[%
  style=ext-authoryear,language=british,giveninits=true,
  uniquename=allinit,urldate=comp,date=year,dateabbrev=false,
  maxbibnames=9,maxcitenames=2]{biblatex}

% remove parentheses from year
% https://tex.stackexchange.com/a/428209
\DeclareFieldFormat{biblabeldate}{#1}
\DeclareDelimFormat[bib]{nameyeardelim}{\adddot\space}
% remove 'In:' before journal title
% https://tex.stackexchange.com/a/10686
\renewbibmacro{in:}{%
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}
}
% modify volume/issue/page number format
% https://tex.stackexchange.com/a/541072
\renewcommand*{\volnumdelim}{\addnbspace}
\DeclareFieldFormat[article,periodical]{number}{\mkbibparens{#1}}
\renewcommand*{\bibpagespunct}{\addcolon\space}
\DeclareFieldFormat{pages}{#1}
% add hyperlinks to \citeyear command
\DeclareCiteCommand{\citeyear}{%
  \usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
  \printtext[bibhyperref]{\usebibmacro{citeyear}}}
  {\multicitedelim}
  {\usebibmacro{postnote}}
\DeclareCiteCommand*{\citeyear}{%
  \usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
  \printtext[bibhyperref]{\usebibmacro{citeyear}\printfield{extradate}}}
  {\multicitedelim}
  {\usebibmacro{postnote}}

% add Oxford comma in the authors list in the bibliography
\DefineBibliographyExtras{british}{\def\finalandcomma{\addcomma}}
% suppress URL, ISSN if DOI is present
\DeclareSourcemap{%
\maps[datatype=bibtex]{\map[overwrite]{%
  \step[fieldsource=doi,final]
  \step[fieldset=url,null]
  \step[fieldset=issn,null]
}}}
% categorise all cited entries
\DeclareBibliographyCategory{cited}
\AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}

% PDF metadata and hyperlink set-up
\usepackage[hidelinks]{hyperref}
\hypersetup{%
  pdftitle={\thetitle},pdfauthor={\theauthor},pdfsubject={\subject},
  pdfkeywords={\keywords},colorlinks=true,linkcolor=\internallink,
  citecolor=\internallink,urlcolor=\externallink
}
% don't use monospace font for URLs
\urlstyle{same}

% glossaries
\usepackage[nopostdot,nonumberlist,symbols,acronyms,style=index,section=subsection]{glossaries}
\renewcommand*{\glossaryname}{Terms}
\renewcommand*{\acronymname}{Abbreviations}
\makeglossaries

% set the document's paper and margin sizes
\usepackage[a4paper]{geometry}
\geometry{%
  lmargin=2.5cm,rmargin=2.5cm,tmargin=2.5cm,bmargin=2.5cm,
  headheight=14.5pt
}

% modify header and footer
\usepackage{fancyhdr}
\usepackage{zref-totpages}
\pagestyle{fancy}
% sets both header and footer to nothing
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyhead[LO,RE]{\textit{\small\shorttitle}}
\fancyfoot[C]{\small\thepage}
% footer for first page
\fancypagestyle{plain}{%
  \fancyhf{}
  % page numbers are disabled if the document is only one page long
  \fancyfoot[C]{%
    \ifnum\ztotpages=1
    \else
    {\small\thepage}
    \fi
  }
}
